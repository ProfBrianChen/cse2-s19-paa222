//Patrick Adams, paa222
// This program picks a random card from the deck so one can practice card tricks alone
import java.util.*;
import java.util.Arrays;
import java.util.List;

public class CardGenerator{ 
    	    // main method required for every program
   	      public static void main(String[] args) {
         
         //Selecting a numbers from 1 to 52
         int card=(int) ((Math.random()*52) +1);
           
         //Initilizing variables
         String suite="";
         String option = "";
         
         
            //Cards suite identity
            if (card>=39 && card<=52)  
            suite= "Spades"; //spades is the suite
            else if (card>=26 && card<=39)  
            suite= "Hearts"; //hearts is the suite
            else if (card>=13 && card<=26)
            suite= "Clubs"; //clubs is the suite
            else if (card<=13)
            suite= "Diamonds"; //diamonds is the suite
         
       //Potential options for the card 
       switch(card%13){  
       case 1:
         option="Ace"; //card option is a ace
         break;
       case 2:
         option="2"; //card option is a 2
         break;
       case 3:
         option="3"; //card option is a 3
         break;
       case 4:
         option="4"; //card option is a 4
         break;
       case 5:
         option="5"; //card option is a 5
         break;   
       case 6:
         option="6"; //card option is a 6
         break;
       case 7:
         option="7"; //card option is a 7
         break;
       case 8:
         option="8"; //card option is a 8
         break;
       case 9:
         option="9"; //card option is a 9
         break;
       case 10:
         option="10"; //card option is a 10
         break;
       case 11:
         option="Jack"; //card option is a jack
         break;
       case 12:
         option="Queen"; //card option is a queen
         break;
       case 13:
         option="King"; //card option is a king
         break;
     }
       //Printing the output of the card picked  
       System.out.println("You picked the " + option + " of " + suite + " ");
         
         
       }
  
}