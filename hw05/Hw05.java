import java.util.Scanner;   
  public class Hw05{      //Start of class 
  public static void main(String[] args){   //Main method
  Scanner schedule = new Scanner(System.in);  //Declaring a new Scanner
  double trash = 2.0;  //declaring the variable 
 
   //Entering the Course Nummber information
   System.out.println("Enter the course number:  "); //Prompts user for the course number   
    while(!schedule.hasNextInt()){      //Checking if user entered an integer
        System.out.println("Error: Enter the course number:  ");    //Printing an error message until an integer is entered
        schedule.next();                                      
    }
        int sectionnumber = schedule.nextInt();  //Stores the course number value
    
    //Entering the number of Times a week the class meets
    System.out.println("Enter how many times the class meets in a week:  ");   //Prompts user for the number of time the class meets in a week 
      while(!schedule.hasNextInt()){  //Checking if user entered an integer
        System.out.println("Error: Enter how many times the class meets in a week:  "); //Printing an error message and prompts you to enter an integer 
        schedule.next();
    }
         int classPerWeek = schedule.nextInt();  //Stores the value
    
    //Entering the number of students in a class
    System.out.println("Enter the number of students:   "); //Prompts the user to enter the number of students 
      while(!schedule.hasNextInt()){  //Checking if an integer was entered
        System.out.println("Error: Enter the number of students:  "); //Printing an error message and prompts you to enter an integer 
        schedule.next();
     }
         int numStudents = schedule.nextInt(); //Stores the value
    
    //Prompts the user for the Department Name
    System.out.println("Enter the department name:   "); //Prompts user for the name of the department
      while (schedule.hasNextDouble()==true) {  //Checking if user entered a string
        trash = schedule.nextDouble();
        System.out.println("Error: Enter the department name:   "); //Printing an error and prompts you to enter a string
    }
        String deptName = schedule.next();  //Stores the value of deptartment name  
    
    //Entering the Professors name
    System.out.println("Enter the professor's name:   ");  //Prompts user for the name of the professor
      while (schedule.hasNextDouble()==true) { //Checking if a string was entered 
        trash = schedule.nextDouble();
        System.out.println("Error: Enter the professor's name:   "); //Printing an error and prompts you to enter a string
    } 
        String professorName = schedule.next(); //Stores the value of professor name
    
    //Entering the class start time
    System.out.println("Enter the class start time:    ");  //Prompts user for the start time of the class
      while (schedule.hasNextDouble()==true) { //Checking if a string was entered
        trash = schedule.nextDouble();
        System.out.println("Error: Enter the class start time:     "); //Printing an error and prompts you to enter a string
    }
       String startTime = schedule.next();  //Stores the value of the class start time
   
    //Printing all of the information 
    System.out.println("The course number is " + sectionnumber + ".");  
    System.out.println("The class meets " + classPerWeek + " times per week.");
    System.out.println("There are " + numStudents + " students in the class.");
    System.out.println("The department is " + deptName + ".");
    System.out.println("The instructor is " + professorName + ".");
    System.out.println("The class starts at " + startTime + ".");
    

    
  }   //ending main method

}  //end of class 