//////////////
//// CSE 02 Hello World 
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Hello, World to terminal window
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------"); 
    System.out.println("   ^  ^  ^  ^  ^  ^"); 
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\"); 
    System.out.println(" <-P--A--A--2--2--2->"); 
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v");                    
  }    
  
}     