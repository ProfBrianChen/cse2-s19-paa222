//Patrick Adams, CSE2
//This program processes a string by examining all the characters, or just a specified number of characters in the string, and determines if they are letters 
import java.util.Scanner ;
import java.util.Random ;
import java.lang.Character ;

public class StringAnalysis{
    public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in) ; //Declaring new scanner named myScanner
    System.out.println("Enter a string"); //Prompts the user to enter a string
    String typedString = myScanner.next() ; //stores the string value
    int stringLength = typedString.length() ;  //stores how many characters are in the string
   

    System.out.println("Enter how many characters should be checked:  "); //Prompts the user to enter a number of characters
    int inputLength = myScanner.nextInt() ; //stores the value entered

    //Checking the string length
    if (stringLength == inputLength){  //If the string length is equal to the length the user inputed print the following
      System.out.println(Everycharacter(typedString)) ;  //Prints o
    }
    else if (stringLength > inputLength) {  //If the string length is larger than the length the user inputed print the following
      System.out.println(Partialcheck(typedString, inputLength )) ; 
    }
    else if (stringLength < inputLength){ //If the string length is smaller than the length the user inputed print the following
      inputLength = inputLength - (inputLength-stringLength) ;  
      System.out.println(Partialcheck(typedString, inputLength )) ;
    }
  }

 
    //New method checking for all characters
    public static boolean Everycharacter(String stringInput){ //For when all characters are being checked
      int stringLength = stringInput.length() ; //stores the string value
      boolean stringOne = true ;
      for(int i =0; i<stringLength; i++){ 
        char specificCharacter = stringInput.charAt(i) ;
        boolean boolOne = Character.isLetter(specificCharacter) ; 
     //Checking the values entered   
     if (boolOne == false ){
         stringOne = false ;
     break ;
          }
        else if (boolOne == true ){
          stringOne = true ;
       }      
  }
    return stringOne ; //Returns the code to the next line
  }
  

   //New method for checking partial characters
   public static boolean Partialcheck(String typedstring, int input){ //For when only part of the characters are being checked
     String stringInput = " ";
     boolean stringOne = true ; 
      for(int i=0; i<=input ; i++){
        char specificCharacter=stringInput.charAt(i) ;
        boolean boolOne = Character.isLetter(specificCharacter) ;
   //Checking the values entered
     if (boolOne == false ){
        stringOne = false ;
     break ;
      }
     else if (boolOne == true ){
        stringOne = true ;      
      }       
  }
   return stringOne ; //Returns the code to the next line
  }
}
