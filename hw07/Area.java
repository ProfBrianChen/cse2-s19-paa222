//Patrick Adams, CSE02
//This program prompts the user to enter the shape of triangle, circle, or rectangle then after prompting for dimensions calculates the area

import java.util.Scanner;
import java.util.Random;
import java.lang.Math;

public class Area{
    public static double triArea( double base, double height){ //Declaring the method for triangle
    double area = base * height / 2; //Stating the formula for the area of a triangle
    return area;  //Goes to the next line
  }
    public static double rectArea(double width, double length){  ///Declaring the method for the shape option, rectangle 
    double area = length * width ; //Stating the formula for the area of a rectangle
    return area; //Goes to the next line 
  } 
    public static double circleArea( double radius){ //Declaring the method for the circle
    double area = Math.PI * Math.pow(radius,2) ; //Declaring the formula for the area of a circle
    return area; //Goes to the next line
  }
  public static void main(String[] args){ //Main method for the program
    Scanner myScanner = new Scanner(System.in) ;//Declaring new scanner named myScanner
    System.out.println("Enter the shape of choice:  ") ; //Prompting the user to enter a shape
    String myShape = myScanner.next() ;  
    
    
    //Rectangle Shape Method
    if (myShape.equals("rectangle")){  //If the user enter Rectangle
      System.out.println("Enter a double for the length of the rectangle:   ") ; //Prompts the user to enter a double
      double rectLength = myScanner.nextDouble(); //Stores the value
      
      System.out.println("Enter a double for the length of the rectangle:   ") ; //Prompts the user to enter a double
      double rectWidth = myScanner.nextDouble(); //Stores the value
      System.out.println(rectArea(rectLength,rectWidth)); //Prints out the area of the Rectangle 
    }
    
    //Triangle shape method
    else if (myShape.equals("triangle")){ //If the user enters a triangle
      System.out.println("Enter a double for the base of the triangle:  ") ; //Prompts the user to enter a double
      double triBase = myScanner.nextDouble(); //Stores the base

      System.out.println("Enter a double for the height of the triangle:  ") ; //Prompts the user to enter a double
      double triHeight = myScanner.nextDouble();  //stores the height value
      System.out.println(triArea(triBase,triHeight)); //Prints and calculates the triangles area
    }
    
    //Circle Shape Method
    else if (myShape.equals("circle")){ //If the user enter Circle
      System.out.println("Enter a double for the radius of the circle:"); //Prompts the user to enter a double
      double Radius = myScanner.nextDouble(); //Stores the value
      System.out.println(circleArea(Radius)); //Print and calculates the value
    }
    

    else {  
      //Prompts an error if the user doesn't enter a valid shape
      System.out.println("Error: This program doesn't read that shape, Please enter a valid shape ") ;
      while(true){
         System.out.println("Enter the shape of choice:  ") ;
         myShape = myScanner.next() ;
        
       //Rectangle area method, same as original rectangle method 
       if (myShape.equals("rectangle")){
        System.out.println("Enter a double for the length of the rectangle:   ") ; //Prompts the user to enter a double
        double rectLength = myScanner.nextDouble(); 
        System.out.println("Enter a double for the length of the rectangle:   ") ; 
        double rectWidth = myScanner.nextDouble();
        System.out.println(rectArea(rectLength,rectWidth)); 
       break;
      }
        
      //Circle area method  
      else if (myShape.equals("circle")){
        System.out.println("Enter a double for the radius of the circle:"); //Prompts the user to enter a double
        double Radius = myScanner.nextDouble(); 
        System.out.println(circleArea(Radius)); //Print and calculates the value
      break;
      }
        
      //Triangle area method, same as original triangle method   
      else if (myShape.equals("triangle")){
        System.out.println("Enter a double for the base of the triangle:  ") ; //Prompts the user to enter a double
        double triBase = myScanner.nextDouble(); 
        System.out.println("Enter a double for the height of the triangle:  ") ; 
        double triHeight = myScanner.nextDouble(); 
        System.out.println(triArea(triBase,triHeight)); 
       break;
      }

      }
    }
  }
}
          