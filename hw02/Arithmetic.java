public class Arithmetic{
  
  public static void main(String args[]){
//Number of pairs of pants 
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of shirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

//data    
double totalCostOfPants;  //total cost of pants
double totalCostOfShirts; //total cost of shirts
double totalCostOfBelts; //total cost of belts
double salestaxOfPants; //sales tax of pants
double salestaxOfShirts; //sales tax of shirts
double salestaxOfBelts; //sales tax of belts
double totalcostbeforetax; //total cost before tax
double totalsalestax; //total sales tax
double totalcostaftertax; //total cost after tax

//Equations for the data
totalCostOfPants=numPants*pantsPrice; //total cost of pants
totalCostOfShirts=numShirts*shirtPrice; //total cost of shirts
totalCostOfBelts=numBelts*beltCost; //total cost of belts
salestaxOfPants=totalCostOfPants*paSalesTax; //sales tax for cost of pants
salestaxOfShirts=totalCostOfShirts*paSalesTax; //sales tax for cost of shirts
salestaxOfBelts=totalCostOfBelts*paSalesTax; //sales tax for total cost of belts
totalcostbeforetax=totalCostOfPants+totalCostOfShirts+totalCostOfBelts; //total cost before tax
totalsalestax=salestaxOfPants+salestaxOfShirts+salestaxOfBelts; //total sales tax
totalcostaftertax=totalcostbeforetax+totalsalestax; //total cost after taxes 
  
salestaxOfPants=salestaxOfPants*100+0.5;
salestaxOfShirts=salestaxOfShirts*100+0.5;
salestaxOfBelts=salestaxOfBelts*100+0.5;
totalsalestax=totalsalestax*100+0.5;
totalcostaftertax=totalcostaftertax*100+0.5;
  
salestaxOfPants=(int)salestaxOfPants;
salestaxOfPants/=100.0;
salestaxOfShirts=(int)salestaxOfShirts;
salestaxOfShirts/=100.0;
salestaxOfBelts=(int)salestaxOfBelts;
salestaxOfBelts/=100.0;
totalsalestax=(int)totalsalestax;
totalsalestax/=100.0;  
totalcostaftertax=(int)totalcostaftertax;
totalcostaftertax/=100.0;
  
//print out the data
System.out.println("total cost of pants is $"+totalCostOfPants); 
System.out.println("total cost of shirts is $"+totalCostOfShirts);
System.out.println("total cost of belts is $"+totalCostOfBelts);
System.out.println("sales tax for cost of pants is $"+salestaxOfPants);
System.out.println("sales tax for cost of shirts is $"+salestaxOfShirts);
System.out.println("sales tax for total cost of belts is $"+salestaxOfBelts);
System.out.println("total cost before tax is $"+totalcostbeforetax);
System.out.println("total sales tax is $"+totalsalestax);
System.out.println("total cost after taxes is $"+totalcostaftertax);  
    
    }
  
} 
