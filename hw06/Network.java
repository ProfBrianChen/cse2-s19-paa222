//Patrick Adams hw06
//This code prompts the user for dimensions of a pattern and it creates a square
import java.util.Scanner;   //importing a scanner 
public class Network{  //class method
  public static void main(String[] args){  //main method
  Scanner myPattern = new Scanner(System.in);   //declaring a new scanner
    
    //Declaring the pattern variables
    int width = 0;  //initializing the width
    int height = 0;  //initializing the height
    int edgeLength = 0;  //initializing the edge length
    int patternSize = 0;  //initializing the square size
   
    
    
    //Determining the width
      System.out.print("Enter the patterns width: "); //Asks for the user to input a width
    	while(true){ //Accepts input if integer is entered 
    	while(myPattern.hasNextInt() == false){
      	System.out.print("Error: Enter an integer: "); //Checks if an integer was entered and prompts for the user to input an integer
      	myPattern.next();  //Deletes invalid data
        }
        width = myPattern.nextInt(); //stores the width
        if(width>0){  //Checks if a positive integer was entered
        break; //Checks if a positive integer was entered 
      }
      System.out.print("Error: Enter a positive integer: "); //States an error and prompts for a positive width
    }

                       
    //Determining the height
    System.out.print("Enter the patterns height:  ");//Asks for the user to input a height
    while(true){  //Accepts input if integer is entered 
    while(myPattern.hasNextInt() == false){ 
      System.out.print("Error: Enter an integer: "); //States an error if the input wasn’t an Integer
      myPattern.next(); //Deletes the unnecessary input 
      }
      height = myPattern.nextInt(); //stores the height
      if(height>0){
      break;  //Checks if a positive integer was entered 
      }
     System.out.print("Error: Enter a positive integer:  ");  //States an error and prompts the user for a positive integer
    }
    
     
                      
  //Determining the edge length
    System.out.print("Enter an edge length: "); //Asks for the user to input an edge length
    while(true){ //Accepts input if integer is entered
    while(myPattern.hasNextInt() == false){ 
      System.out.print("Error: Please type in an integer: ");//States an error if the input wasn’t an integer 
      myPattern.next();  //Deletes invalid data
      }
      edgeLength = myPattern.nextInt(); //stores the data
      if(edgeLength>0){ //Checks if a positive integer was entered
      break; // 
      }
    System.out.print("Error: Enter in a positive number: ");//States an error and promptsthe user for a positive integer
    }
    
                     
                     
    //Determining the square size
    System.out.print("Enter the patterns square size: ");//Asks for the user to input a square size
    while(true){ //Accepts input if integer is entered
    while(myPattern.hasNextInt() == false){
      System.out.print("Error: Please type in an integer: "); //States an error if the input wasn’t an Integer      
      myPattern.next(); //Deletes invalid data
      }
      patternSize = myPattern.nextInt(); //Stores the size of the pattern
      if(patternSize>0){ //Checks if a positive integer was entered
        break; 
      }
    System.out.print("Error: Enter a positive number: "); //States an error and prompts the user for a positive integer
    }
    
    
    //Printing the pattern
    int width1 = 0; //Width of square
    int height1 = 0; //Height of square
    int x = 0;
    int p = patternSize%2; //Checking if the square size is odd or even 
    int edge1 = 0; //Prints out both even and odd number pattern sizes
    int edge2 = 0;//Prints out only even number pattern sizes
    
    if(p==0){  //When the square size is an even number, edge line 1 and 2 move to the middle of   the square
      edge1 =  (patternSize-1)/2; //edge line 1 moving to the middle of the page
      edge2 = edge1+1; //edge line 2 moving to the middle of the page
    }
    else{ //When the square size is odd, only edge line 1 moves to the middle
      edge1 = (patternSize-1)/2;
    }
//System.out.println(edgeLine1);
//System.out.println(edgeLine2);

    
    
  do{
    //Declaring symbols to for the pattern
    height1 = x%(patternSize+edgeLength);
    
    for(int j = 0; j<width;j++){
      width1 = j%(patternSize+edgeLength);
      //System.out.print(width1);
      //System.out.print(height1);

      //if width1 belongs to the square part
      if(width1 < patternSize){//if this is less than the square size
        if(height1 <patternSize){//if this is the square part
        
        if(height1 == 0 || height1 == patternSize-1 ){ //States the width of the pattern
          if(width1 == 0 || width1 == patternSize-1){ //States the corners of the pattern
            System.out.print("#");//The symbol “#” is printed when on the corner of the pattern 
          }
          else{ //The sides of the pattern
            System.out.print("-") ;//The symbol “-” is printed when on the top and bottom of the 			square 
          }
          
        }
        else{//The height or sides of the pattern
          if(width1 == 0 || width1 == patternSize-1){ //Printing on the sides of the pattern
            System.out.print("|");  //Printing out “I” on the sides of the pattern
          }
          else{ //Printing inside the pattern
            System.out.print(" "); //Printing out empty space within the square
          }
          
        }
       }
        else{
          if(p==0){//Stating when the patternSize is an even number
            if(width1 == edge1 || width1 == edge2){ //When width1 is on the line
              System.out.print("|");//Print out “I” if the width is on the edge line
              
            }
            else{ 
              System.out.print(" ");//When the width is not on the line, print out white space
            }
            
          }
          else{//Stating when the patternSize is an odd number
            if(width1 == edge1){
              System.out.print("|");//Print out “I” if width is on the line
            }
            else{
              System.out.print(" ");//If the width isn’t on the line, print out white space
            }
            
          }
          
        }
      }
      
      else{
        if(p==0){//if the patternsize is an even number
          if(height1 == edge1 || height1 == edge2){
            System.out.print("-");//If the inputed height is on the edge line then print “-“
          }
          else{
            System.out.print(" ");//If not on the edge then print out blank space 
          }
         
        }
        else{//Pattern is odd numbered
          if(height1 == edge1){//
            System.out.print("-"); //If the height is on the edge print “-“
          }
          else{
            System.out.print(" ");//Prints out blank space between the square if the height is not on the edge
          } 
        }   
      }          
    }
    System.out.println("");//This code moves to the next lne
    x++;//Add one to x 
    
    
  }while(x<height);  //Print out the pattern
    
  }
  
}
