import java.util.*;
import java.util.Arrays;
import java.util.List;

public class PokerHandCheck{
    public static void main(String[] args){
        
   //Numbers from 1 to 52
   int card1=(int) ((Math.random()*52) +1);
   int card2=(int) ((Math.random()*52) +1);
   int card3=(int) ((Math.random()*52) +1);
   int card4=(int) ((Math.random()*52) +1);
   int card5=(int) ((Math.random()*52) +1);
   
    //Initializing identities and suites
    String suite1="";
    String suite2="";
    String suite3="";
    String suite4="";
    String suite5="";
    String option1 = "";
    String option2= "";
    String option3 = "";
    String option4 = "";
    String option5 = "";
    String hand;
   
   //Card 1 suite identity
   if (card1>=39 && card1<=52)  
   suite1= "Spades"; //spades is the suite
   else if (card1>=26 && card1<=39)  
   suite1= "Hearts"; //hearts is the suite
   else if (card1>=13 && card1<=26)
    suite1= "Clubs"; //clubs is the suite
    else if (card1<=13)
    suite1= "Diamonds"; //diamonds is the suite
    //Card 2 suite identity
    if (card2>=39 && card2<=52)  
    suite2= "Spades"; //spades is the suite
    else if (card2>=26 && card2<=39)  
    suite2= "Hearts"; //hearts is the suite
    else if (card2>=13 && card2<=26)
    suite2= "Clubs"; //clubs is the suite
    else if (card2<=13)
    suite2= "Diamonds"; //diamonds is the suite   
    //Card 3 suite identity
    if (card3>=39 && card3<=52)  
    suite3= "Spades"; //spades is the suite
    else if (card3>=26 && card3<=39)  
    suite3= "Hearts"; //hearts is the suite
    else if (card3>=13 && card3<=26)
     suite3= "Clubs"; //clubs is the suite
     else if (card3<=13)
      suite3= "Diamonds"; //diamonds is the suite
     //Card 4 suite idenity
     if (card4>=39 && card4<=52)  
     suite4= "Spades"; //spades is the suite
     else if (card4>=26 && card4<=39)  
     suite4= "Hearts"; //hearts is the suite
     else if (card4>=13 && card4<=26)
     suite1= "Clubs"; //clubs is the suite
     else if (card4<=13)
     suite4= "Diamonds"; //diamonds is the suite  
     //Card 5 suite identity   
     if (card5>=39 && card5<=52)  
     suite5= "Spades"; //spades is the suite
     else if (card5>=26 && card5<=39)  
     suite5= "Hearts"; //hearts is the suite
     else if (card5>=13 && card5<=26)
     suite5= "Clubs"; //clubs is the suite
     else if (card5<=13)
     suite5= "Diamonds"; //diamonds is the suite  
  
      
     //Potential options for card 1
     switch(card1%13){  
       case 1:
         option1="Ace"; //card 1 option is a ace
         break;
       case 2:
         option1="2"; //card 1 option is a 2
         break;
       case 3:
         option1="3"; //card 1 option is a 3
         break;
       case 4:
         option1="4"; //card 1 option is a 4
         break;
       case 5:
         option1="5"; //card 1 option is a 5
         break;   
       case 6:
         option1="6"; //card 1 option is a 6
         break;
       case 7:
         option1="7"; //card 1 option is a 7
         break;
       case 8:
         option1="8"; //card 1 option is a 8
         break;
       case 9:
         option1="9"; //card 1 option is a 9
         break;
       case 10:
         option1="10"; //card 1 option is a 10
         break;
       case 11:
         option1="Jack"; //card 1 option is a jack
         break;
       case 12:
         option1="Queen"; //card 1 option is a queen
         break;
       case 13:
         option1="King"; //card 1 option is a king
         break;
     }
      
     //Potential options for card 2
     switch(card2%13){  
       case 1:
         option2="Ace"; //card 2 option is a ace
         break;
       case 2:
         option2="2"; //card 2 option is a 2
         break;
       case 3:
         option2="3"; //card 2 option is a 3
         break;
       case 4:
         option2="4"; //card 2 option is a 4
         break;
       case 5:
         option2="5"; //card 2 option is a 5
         break;   
       case 6:
         option2="6"; //card 2 option is a 6
         break;
       case 7:
         option2="7"; //card 2 option is a 7
         break;
       case 8:
         option2="8"; //card 2 option is a 8
         break;
       case 9:
         option2="9"; //card 2 option is a 9
         break;
       case 10:
         option2="10"; //card 2 option is a 10
         break;
       case 11:
         option2="Jack"; //card 2 option is a jack
         break;
       case 12:
         option2="Queen"; //card 2 option is a queen
         break;
       case 13:
         option2="King"; //card 2 option is a king
         break;
     }
      
     //Potential options for card 3
     switch(card3%13){  
       case 1:
         option3="Ace"; //card 3 option is a ace
         break;
       case 2: 
         option3="2"; //card 3 option is a 2
         break;
       case 3:
         option3="3"; //card 3 option is a 3
         break;
       case 4:
         option3="4"; //card 3 option is a 4
         break;
       case 5:
         option3="5"; //card 3 option is a 5
         break;   
       case 6:
         option3="6"; //card 3 option is a 6
         break;
       case 7:
         option3="7"; //card 3 option is a 7
         break;
       case 8:
         option3="8"; //card 3 option is a 8
         break;
       case 9:
         option3="9"; //card 3 option is a 9
         break;
       case 10:
         option3="10"; //card 3 option is a 10
         break;
       case 11:
         option3="Jack"; //card 3 option is a jack
         break;
       case 12:
         option3="Queen"; //card 3 option is a queen
         break;
       case 13:
         option3="King"; //card 3 option is a king
         break;
     }
      
     //Potential options for card 4
     switch(card4%13){  
       case 1:
         option4="Ace"; //card 4 option is a ace
         break;
       case 2:
         option4="2"; //card 4 option is a 2
         break;
       case 3:
         option4="3"; //card 4 option is a 3
         break;
       case 4:
         option4="4"; //card 4 option is a 4
         break;
       case 5:
         option4="5"; //card 4 option is a 5
         break;   
       case 6:
         option4="6"; //card 4 option is a 6
         break;
       case 7:
         option4="7"; //card 4 option is a 7
         break;
       case 8:
         option4="8"; //card 4 option is a 8
         break;
       case 9:
         option4="9"; //card 4 option is a 9
         break;
       case 10:
         option4="10"; //card 4 option is a 10
         break;
       case 11:
         option4="Jack"; //card 4 option is a jack
         break;
       case 12:
         option4="Queen"; //card 4 option is a queen
         break;
       case 13:
         option4="King"; //card 4 option is a king
         break;
     }
      
     //Potential options for card 5
     switch(card5%13){  
       case 1:
         option5="Ace"; //card 5 option is a ace
         break;
       case 2:
         option5="2"; //card 5 option is a 2
         break;
       case 3:
         option5="3"; //card 5 option is a 3
         break;
       case 4:
         option5="4"; //card 5 option is a 4
         break;
       case 5:
         option5="5"; //card 5 option is a 5
         break;   
       case 6:
         option5="6"; //card 5 option is a 6
         break;
       case 7:
         option5="7"; //card 5 option is a 7
         break;
       case 8:
         option5="8"; //card 5 option is a 8
         break;
       case 9:
         option5="9"; //card 5 option is a 9
         break;
       case 10:
         option5="10"; //card 5 option is a 10
         break;
       case 11:
         option5="Jack"; //card 5 option is a jack
         break;
       case 12:
         option5="Queen"; //card 5 option is a queen
         break;
       case 13:
         option5="King"; //card 5 option is a king
         break;
     }
         
  //Stating all possible one pair combinations
    //card 1     
    if (card1==card2 && card1!=card3 && card1!=card4 && card1!=card5)
    hand="Pair";
    //card 1     
    else if (card1==card3 && card1!=card2 && card1!=card4 && card1!=card5)
    hand="Pair";
    //card 1     
    else if (card1==card4 && card1!=card2 && card1!=card3 && card1!=card5)
    hand="Pair";
    //card 1   
    else if (card1==card5 && card1!=card2 && card1!=card3 && card1!=card4)
    hand="Pair";
    //Card 2
    else if (card2==card3 && card2!=card1 && card2!=card4 && card2!=card5)
    hand="Pair";
    //card 2     
    else if (card2==card4 && card2!=card1 && card2!=card3 && card2!=card5)
    hand="Pair";
    //card 2      
    else if (card2==card5 && card2!=card1 && card2!=card3 && card2!=card4)
    hand="Pair";
    //Card 3
    else if (card3==card4 && card3!=card1 && card3!=card2 && card3!=card5)
    hand="Pair";
    //Card 3     
    else if (card3==card5 && card3!=card1 && card2!=card3 && card3!=card4)
    hand="Pair";
    //Card 4
    else if (card4==card5 && card4!=card1 && card2!=card4 && card3!=card4)
    hand="Pair";  
    
  //Stating all possible two pair combinations
    //card 1 and card 2 are selected
    if (card1==card2 && card3==card4)
    hand ="Two Pair";
    //card 1 and card 2 are selected     
    else if (card1==card2 && card3==card5)
    hand ="Two Pair";
    //card 1 and card 2 are selected     
    else if (card1==card2 && card4==card5)
    hand ="Two Pair";
    //card 1 and card 3
    else if (card1==card3 && card2==card4)
    hand ="Two Pair";
    //card 1 and card 3     
    else if (card1==card3 && card2==card5)
    hand ="Two Pair";
    //card 1 and card 3     
    else if (card1==card3 && card4==card5)
    hand ="Two Pair";
    //card 1 and card 4
    else if (card1==card4 && card2==card3)
    hand ="Two Pair";
    //card 1 and card 4     
    else if (card1==card4 && card2==card5)
    hand ="Two Pair";
    //card 1 and card 4     
    else if (card1==card4 && card3==card5)
    hand ="Two Pair";
    //card 1 and card 5
    else if (card1==card5 && card2==card3)
    hand ="Two Pair";
    //card 1 and card 5     
    else if (card1==card5 && card2==card4)
    hand ="Two Pair";
    //card 1 and card 5     
    else if (card1==card5 && card3==card4)
    hand ="Two Pair";
   //Cart 1 not included possibility  
   else if (card2==card3 && card4==card5)
   hand ="Two Pair";
   else if (card2==card4 && card3==card5)
   hand ="Two Pair";
   else if (card2==card5 && card3==card4)
   hand ="Two Pair";  
         
  //Stating all posibilitties for three pair combinations   
   //Card 1 is selected 
   else if (card1==card2 && card1==card3 && card1!=card4 && card1!=card5)
   hand="Three of a kind";
   //Card 1 is selected 
   else if (card1==card2 && card1==card4 && card1!=card3 && card1!=card5)
   hand="Three of a kind";
   //Card 1 is selected       
   else if (card1==card2 && card1==card5 && card1!=card3 && card1!=card4)
   hand="Three of a kind";
   //Card 1 is selected       
   else if (card1==card3 && card1==card4 && card1!=card2 && card1!=card5)
   hand="Three of a kind";
   //Card 1 is selected       
   else if (card1==card3 && card1==card5 && card1!=card4 && card2!=card2)
   hand="Three of a kind";
   //Card 1 is selected       
   else if (card1==card4 && card1==card5 && card1!=card3 && card1!=card2)
   hand="Three of a kind";
   //Card 2 is selected
   else if (card2==card3 && card2==card4 && card2!=card1 && card2!=card5)
   hand="Three of a kind";
   //Card 2 is selected     
   else if (card2==card3 && card2==card5 && card2!=card4 && card2!=card1)
   hand="Three of a kind";
   //Card 2 is selected        
   else if (card2==card4 && card2==card5 && card2!=card1 && card2!=card3)
   hand="Three of a kind";      
   //Card 3 is selected  
   else if (card3==card4 && card3==card5 && card3!=card1 && card3!=card2)
   hand="Three of a kind";      
   
   //When no pairs are found, the hand is known as "High Card Hand"
   else 
   hand="High Card Hand";
         
   //Printing out the five card combination
   System.out.println("The random cards you picked were:");
   System.out.println("The " + option1 + " of " + suite1 + " ");
   System.out.println("The " + option2 + " of " + suite2 + "");
   System.out.println("The " + option3 + " of " + suite3 + "");
   System.out.println("The " + option4 + " of " + suite4 + "");
   System.out.println("The " + option5 + " of " + suite5 + "");
   System.out.println("You got a " + hand + "!");      
         
     }  
  
}
     




      

        



         