// Prints the number of minutes for each trip, number of counts for each trip, 
// Prints the distance of each trip in miles and distance for the two trips combined.
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    // store the number of seconds for each trip and the number of counts for each trip
    int secsTrip1=480; // First trip was 480 seoncds
    int secsTrip2=3220; // Second trip was 3220 seconds
    int countsTrip1=1561; // The number of rotations in the first trip was 1561 
	  int countsTrip2=9037; // Provides the number of rotations in the second trip
    // Variables for useful constants and for storing the various distances. Output data
    double wheelDiameter=27.0,  //Provides the wheel diameter
  	PI=3.14159, //Provides the value of Pi
  	feetPerMile=5280,  //Provides how many feet are in a mile
  	inchesPerFoot=12,   //Inches in a foot
  	secondsPerMinute=60;  //Provides the value of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //Doubles the values of the trip 
    //Printing the following statements
    System.out.println("Trip 1 took "+
         (secsTrip1/secondsPerMinute)+" minutes and had "+
       	  countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	 (secsTrip2/secondsPerMinute)+" minutes and had "+
       	  countsTrip2+" counts.");
    //Computing the calculations
    distanceTrip1=countsTrip1*wheelDiameter*PI; //Equation for distance of trip one
    // Above gives distance in inches
    // A rotation of the wheel travels the diameter in inches times PI
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Equation for trip one
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Equation for trip two
	  totalDistance=distanceTrip1+distanceTrip2; //Equation that determines the total distance in both trips
    // Printing the following statements
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class