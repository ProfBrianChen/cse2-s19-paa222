// Importing the scanner necessary for program
import java.util.Scanner;
//Determine how much each person in the group needs to spend in order to pay the check
public class Check{ 
    	 // main method required for every program
   	   public static void main(String[] args) {
       Scanner myScanner = new Scanner( System.in ); //Declaring an instance of the Scanner object and call the Scanner constructor
       System.out.print("Enter the original cost of the check in the form xx.xx: "); //printing out the followin statement
       double checkCost = myScanner.nextDouble(); //Accepting the user input
       System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //printing out the following statement
            double tipPercent = myScanner.nextDouble(); //Accepting the user input
            tipPercent /= 100; // Convert the percentage into a decimal value
       //Prompt the user for the number of people that went to dinenr  
       System.out.print("Enter the number of people who went out to dinner: " ); 
       int numPeople = myScanner.nextInt();
       //Output the amount that each member of the group needs to spend in order to pay the check  
       double totalCost;
       double costPerPerson;
       int dollars,   //whole dollar amount of cost 
       dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
       totalCost = checkCost*(1 + tipPercent);
       costPerPerson = totalCost/numPeople;
       //get the whole amount, dropping decimal fraction
       dollars=(int)costPerPerson;
       //get dimes amount, e.g., 
       // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
       //  where the % (mod) operator returns the remainder
       //  after the division:   583%100 -> 83, 27%5 -> 2 
       dimes=(int)(costPerPerson * 10) % 10;
       pennies=(int)(costPerPerson * 100) % 10;
       //Printing out the amount each person in the group owes  
       System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); 

   
   
}  //end of main method   
  	} //end of class