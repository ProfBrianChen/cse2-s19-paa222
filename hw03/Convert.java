//The program converts meters to inches when users inputs a certain amount of meters
import java.util.Scanner;
public class Convert{
  //method required for every java program
  public static void main(String args[]){
  Scanner myScanner = new Scanner(System.in); //Giving the system a specific name
  System.out.print("Enter the distance in meters: "); //printing out the following phrase, "Enter the distance in meters" 
  double meters = myScanner.nextDouble(); //allowing for java to solve the equation
  double inches = meters*39.3701; //assigning a way for java to complete the equation
  System.out.print(meters +" meters is "); //print meters given
  System.out.printf("%.4f", inches); //allowing the decimal point to be correct
  System.out.println(" inches."); //printing inchess

        } //end of main method
  
} //end of class