//This program gives the volume of a box when given dimensions
import java.util.Scanner;
public class BoxVolume{
  //method required for every java program
  public static void main(String args[]){
  Scanner myScanner= new Scanner (System.in); //assigning a specific name 
  System.out.print("The width side of the box is: "); //printing the listed phrase, "The width side of the box is"
  double width = myScanner.nextDouble(); //allowing the user to enter a width
  System.out.print("The length  of the box is: "); //printing the listed phrase, "The length of the box is"
  double length = myScanner.nextDouble(); //allowing the user to enter a length
  System.out.print("The height of the box is: "); //printing the listed phrase, "The height of the box is"
  double height = myScanner.nextDouble(); //allowing the user to enter a height
  System.out.println(); //printing an empty line
  int volume = (int) (width*length*height); //assigning java a equation to use for the data
  System.out.println("The volume inside the box is: "+ volume); //printing the answer to the equation
      
        }
  
} 